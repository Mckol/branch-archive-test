# Branch Archive Test

A test of a branch archiving concept using custom refs.

## The initial document about this

### Introduction
It looks like it's possible to use custom refs to "archive" branches - remove them from the branch list but allow their restoration if needed.

### Technical details
Basically you could have something like `refs/archive/2021/cool-branch` (the `/year/` part is optional but I think it's neat) instead of the branch, which could be turned back into a branch at any point if needed. This works because branches are basically references to the last commit of a branch. As long as a reference to a commit exists, git won't delete it and it's parent commits. And refs can be things like tags, branches, but also just an unspecified something else as shown above.

### Example commands for working with this idea
- Create the archive ref - `git update-ref refs/archive/2021/cool-branch cool-branch`
- List all archived branches - `git for-each-ref -p refs/archive`
- Restore the archived branch - `git checkout -b cool-branch refs/archive/2021/cool-branch`
- Push the archive refs - `git send-pack $(git remote get-url origin) "refs/archive/*"`
- Pull the archive refs - `git fetch origin "+refs/archive/*:refs/archive/*"`
- Pull the archive refs (override local) - `git fetch --prune <remote> "+refs/archive/*:refs/archive/*"`

### Questions
- [?]

### Resources
- Documentation of the `git update-ref` command: <https://git-scm.com/docs/git-update-ref>
- StackOverflow answer which I stole the idea from: <https://stackoverflow.com/questions/25169440/remove-hide-git-branches-without-deleting-commit-histories>
